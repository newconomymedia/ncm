const mix = require('laravel-mix');

mix
  .copyDirectory('web/content/themes/theme/assets/images', 'web/content/themes/theme/dist/images')
  .copyDirectory('web/content/themes/theme/assets/fonts', 'web/content/themes/theme/dist/fonts')
  .sass('web/content/themes/theme/assets/styles/main.scss', 'web/content/themes/theme/dist/styles')
  .js('web/content/themes/theme/assets/scripts/main.js', 'web/content/themes/theme/dist/scripts')
  .js('web/content/themes/theme/assets/scripts/customizer.js', 'web/content/themes/theme/dist/scripts')
  .sourceMaps()
  .options({
    processCssUrls: false
  });
