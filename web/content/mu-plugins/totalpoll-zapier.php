<?php

add_action( 'totalpoll/actions/after/poll/command/log', function ($log, $poll){

    $poll_title = $poll->getTitle();

    $poll_question = ($poll->getQuestions())[0]['content'];

    $poll_choices = $poll->getChoices();
    $poll_labels = [];
    $user_choices = [];
    if (is_array($poll_choices)) {
        foreach ($poll_choices as $poll_choices_item) {
            $poll_labels[] = $poll_choices_item['label'];
            $user_choices[] = $poll_choices_item['receivedVotes'];
        }
    }

    $user_id = $log->getUserId();
    $user_login = $log->getUser()->user_login;
    $user_name = $log->getUser()->display_name;
    $user_email = $log->getUser()->user_email;
    $user_ip = $log->getIp();

    $date = new \DateTime();
    $date->modify('monday this week');
    $current_monday = $date->format('dmY');

    if ($poll_title === NCM_POLL_COMPANIES_FIRST) {
        $poll_id = 'companies-first-' . $current_monday;
        $curlopt_url = NCM_POLL_COMPANIES_FIRST_ZAPIER;
    }  elseif ($poll_title === NCM_POLL_COMPANIES_SECOND) {
        $poll_id = 'companies-second-' . $current_monday;
        $curlopt_url = NCM_POLL_COMPANIES_SECOND_ZAPIER;
    } elseif ($poll_title === NCM_POLL_EXPERTS_FIRST) {
        $poll_id = 'experts-first-' . $current_monday;
        $curlopt_url = NCM_POLL_EXPERTS_FIRST_ZAPIER;
    } elseif ($poll_title === NCM_POLL_EXPERTS_SECOND) {
        $poll_id = 'experts-second-' . $current_monday;
        $curlopt_url = NCM_POLL_EXPERTS_SECOND_ZAPIER;
    } else  {
        return;
    }

    // Configure curl options
    $data = [
        'poll_id' => $poll_id,
        'poll_title' => $poll_title,
        'poll_question' => $poll_question,
        'poll_labels' => serialize(json_encode($poll_labels)),
        'user_id' => $user_id ? $user_id : null,
        'user_login' => $user_login,
        'user_name' => $user_name,
        'user_email' => $user_email,
        'user_ip' => $user_ip,
        'user_choices' => serialize(json_encode($user_choices)),
    ];
    $jsonEncodedData = json_encode($data);

    $opts = array(
        CURLOPT_URL             => $curlopt_url,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_CUSTOMREQUEST   => 'GET',
        CURLOPT_POST            => 1,
        CURLOPT_POSTFIELDS      => $jsonEncodedData,
        CURLOPT_HTTPHEADER  => array('Content-Type: application/json','Content-Length: ' . strlen($jsonEncodedData))
    );

    // Initialize curl
    $curl = curl_init();

    // Set curl options
    curl_setopt_array($curl, $opts);

    // Get the results
    $result = curl_exec($curl);

    // Close resource
    curl_close($curl);
}, 10, 2);
