<?php
/**
 * Plugin Name: Newconomy RSS Feed
 * Plugin URI:  https://newconomy.media/
 * Description: Custom RSS for mobile app.
 * Version:     1.0
 * Author:      Zorca
 * Author URI:  https://newconomy.media
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * Copyright (C) 2018 Newconomy
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace Newconomy\NewconomyRSSFeed;

/**
 * Registers our custom feed
 */
function register() {
	add_feed( 'mobile-apps', __NAMESPACE__ . '\generate_content' );
}
add_action( 'init', __NAMESPACE__ . '\register' );

/**
 * Generates the content of our custom feed
 */
function generate_content() {
    get_template_part('rss', 'mobile-apps');
}
