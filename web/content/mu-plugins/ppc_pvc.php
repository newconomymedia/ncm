<?php
/*
Plugin Name: Post Pay Counter - Post Views Counter integration
Plugin URI: http://www.thecrowned.org/downloads/ppc-pvc
Description: Allows integration of Post Views Counter data with Post Pay Counter.
Author: Stefano Ottolenghi
Version: 1.0
Author URI: http://www.thecrowned.org/
*/

function ppc_pvc_views( $post ) {
	if( function_exists( 'pvc_get_post_views' ) )
		return pvc_get_post_views( $post->ID );
	else
		return 0;
}
?>