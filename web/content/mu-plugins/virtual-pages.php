<?php

add_filter('init', function ($rules) {
    $virtual_pages = [
        'companies' => 'Companies',
        'experts'   => 'Experts'
    ];
    foreach ( $virtual_pages as $slug => $name ) {
        add_rewrite_rule( '^' . $slug . '/([^/]*)/?', 'index.php?virtual_post_type=' . $slug . '&virtual_post_type_slug=$matches[1]', 'top' );
        add_rewrite_tag( '%virtual_post_type_slug%', '([^&]+)' );
    }
});

add_filter('query_vars', function ($vars) {
    $vars[] = 'virtual_post_type';
    $vars[] = 'virtual_post_type_slug';
    return $vars;
}, 0, 1);

function get_company_by_slug($slug) {
    $request = 'https://platform.newconomy.media/api/organizations/' . $slug;
    $response = wp_remote_get($request);
    if (is_wp_error($response)) {
        echo $response->get_error_message();
    }
    return [
        'response_code' => wp_remote_retrieve_response_code($response),
        'response_body' => json_decode(wp_remote_retrieve_body($response), true)[0]
    ];
}

function get_expert_by_slug($slug) {
    $request = 'https://platform.newconomy.media/api/experts/' . $slug;
    $response = wp_remote_get($request);
    if (is_wp_error($response)) {
        echo $response->get_error_message();
    }
    return [
        'response_code' => wp_remote_retrieve_response_code($response),
        'response_body' => json_decode(wp_remote_retrieve_body($response), true)[0]
    ];
}

add_action('wp', function () {
    if (get_query_var('virtual_post_type') && get_query_var('virtual_post_type_slug')) {
        add_filter('wpseo_canonical', '__return_false', 10, 1);
        add_filter('wpseo_json_ld_output', '__return_false', 10, 1);
        add_filter('wpseo_opengraph_url' , '__return_false' );
        add_filter('wpseo_opengraph_desc', '__return_false' );
        add_filter('wpseo_opengraph_title', '__return_false' );
        add_filter('wpseo_opengraph_type', '__return_false' );
        add_filter('wpseo_opengraph_site_name', '__return_false' );
        add_filter('wpseo_opengraph_image' , '__return_false' );
        add_filter('wpseo_opengraph_author_facebook' , '__return_false' );
    }
});

add_filter('wpseo_prev_rel_link', function ($link) {
    if (get_query_var('virtual_post_type') && get_query_var('virtual_post_type_slug')) {
        return false;
    }
    return $link;
});

add_filter('wpseo_next_rel_link', function ($link) {
    if (get_query_var('virtual_post_type') && get_query_var('virtual_post_type_slug')) {
        return false;
    }
    return $link;
});
