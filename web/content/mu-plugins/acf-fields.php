<?php

$video = new \StoutLogic\AcfBuilder\FieldsBuilder('Video Timetable');
$video
    ->addDatePicker('date', ['display_format' => 'Y-m-d', 'return_format' => 'Y-m-d'])
    ->addTimePicker('time', ['display_format' => 'g:i a', 'return_format' => 'g:i a'])
    ->addText('speakers')
    ->setLocation('post_type', '==', 'aiovg_videos')
    ->and('post_taxonomy', '==', 'aiovg_categories:streams');

add_action('acf/init', function() use ($video) {
    acf_add_local_field_group($video->build());
});
