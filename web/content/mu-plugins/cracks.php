<?php

add_action('acf/init', function() {
    delete_option('alm_pro_license_status');
    add_option('alm_pro_license_status', 'valid');
    delete_option('easy_query_license_status');
    add_option('easy_query_license_status', 'valid');
});
