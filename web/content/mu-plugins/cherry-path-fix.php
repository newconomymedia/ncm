<?php

/**
* cherry_core_base_url fix for Bedrock path
*/
add_filter('cherry_core_base_url', function ($url) {
    return str_replace(WP_CONTENT_DIR, '/../content', $url);
});
