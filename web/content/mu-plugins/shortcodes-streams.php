<?php

add_shortcode('streams-timetable', function () {
    $current_date = current_time('Y-m-d');

    ob_start();
    $date_sunday = date('Y-m-d', strtotime('sunday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_sunday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--sunday">
            <div class="streams-timetable__title"><?php echo $date_sunday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_sunday = ob_get_contents();
    ob_end_clean();

    ob_start();
    $date_monday = date('Y-m-d', strtotime('monday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_monday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--monday">
            <div class="streams-timetable__title"><?php echo $date_monday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_monday = ob_get_contents();
    ob_end_clean();

    ob_start();
    $date_tuesday = date('Y-m-d', strtotime('tuesday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_tuesday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--tuesday">
            <div class="streams-timetable__title"><?php echo $date_tuesday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_tuesday = ob_get_contents();
    ob_end_clean();

    ob_start();
    $date_wednesday = date('Y-m-d', strtotime('wednesday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_wednesday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--wednesday">
            <div class="streams-timetable__title"><?php echo $date_tuesday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_wednesday = ob_get_contents();
    ob_end_clean();

    ob_start();
    $date_thursday = date('Y-m-d', strtotime('thursday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_thursday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--thursday">
            <div class="streams-timetable__title"><?php echo $date_thursday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_thursday = ob_get_contents();
    ob_end_clean();

    ob_start();
    $date_friday = date('Y-m-d', strtotime('friday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_friday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--friday">
            <div class="streams-timetable__title"><?php echo $date_friday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_friday = ob_get_contents();
    ob_end_clean();

    ob_start();
    $date_saturday = date('Y-m-d', strtotime('saturday this week'));
    $args = array(
        'posts_per_page'    => 10,
        'post_type'         => 'aiovg_videos',
        'meta_key'          => 'time',
        'orderby' => 'time',
        'meta_type' => 'TIME',
        'order' => 'ASC',
        'meta_query'        => array(
            array(
                'key'   => 'date',
                'compare' => '=',
                'value'   => $date_saturday,
                'type'    => 'DATE'
            ),
        ),
    );
    $the_query = new WP_Query( $args );
    wp_reset_query();
    if( $the_query->have_posts() ): ?>
        <div class="streams-timetable__item streams-timetable__item--saturday">
            <div class="streams-timetable__title"><?php echo $date_saturday; ?></div>
            <div class="streams-timetable-wrap">
                <ul class="streams-timetable-list">
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li class="streams-timetable-list__item l-row l-row--no-gutters">
                            <div class="l-col-auto">
                                <?php
                                $icon_color = '#ED0058';
                                if (current_time('H:i') > get_field('time')) $icon_color = '#06BD85';
                                echo ncm_streams_icon($icon_color);
                                ?>
                            </div>
                            <div class="l-col">
                                <div class="streams-timetable-list__item-wrap">
                                    <a class="streams-timetable-list__link">
                                        <?php the_title(); ?>
                                    </a>
                                    <div class="streams-timetable-list__time"><i class="far fa-clock"></i>&nbsp;<?php the_field('time'); ?></div>
                                    <div class="streams-timetable-list__speakers"><?php the_field('speakers'); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $result_saturday = ob_get_contents();
    ob_end_clean();

    return '<div class="streams-timetable">' . $result_sunday . $result_monday . $result_tuesday . $result_wednesday . $result_thursday . $result_friday . $result_saturday . '</div>';
});

function ncm_streams_icon($color) {
    ob_start();
    ?>
    <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M21.9429 15.8549L15.0905 10.8737C14.8391 10.6914 14.5052 10.664 14.2296 10.8057C13.9518 10.9462 13.7783 11.2317 13.7783 11.5403V21.4993C13.7783 21.8111 13.9518 22.0956 14.2296 22.2361C14.3471 22.2954 14.4756 22.3251 14.6052 22.3251C14.7743 22.3251 14.9456 22.2713 15.0905 22.1647L21.9429 17.188C22.1592 17.0288 22.2855 16.7828 22.2855 16.5214C22.2866 16.2557 22.157 16.0108 21.9429 15.8549Z" fill="<?php echo $color; ?>"/>
        <circle cx="16.5" cy="16.5" r="15.5" stroke="<?php echo $color; ?>" stroke-width="2"/>
        <path d="M25.4031 10.9336C26.382 12.4994 26.9308 14.2956 26.9939 16.1412C27.057 17.9867 26.6323 19.8162 25.7626 21.4452C24.8929 23.0742 23.6089 24.445 22.0403 25.4194C20.4717 26.3937 18.6738 26.9372 16.8281 26.9949C14.9824 27.0526 13.1541 26.6225 11.5277 25.748C9.90124 24.8736 8.53419 23.5856 7.56444 22.0141C6.59468 20.4427 6.05651 18.6432 6.00421 16.7973C5.95192 14.9515 6.38735 13.1245 7.26658 11.5006" stroke="<?php echo $color; ?>" stroke-width="2" stroke-linecap="round"/>
    </svg>
    <?php
    $result = ob_get_contents();
    ob_end_clean();
    return $result;
};

add_shortcode('streams-timetable-old', function () {
    $output = '<div class="streams-timetable"><h5>Live Schedule</h5>';
    if ( have_rows('streams_item', 'option') ) {
        $i = 1;
        while ( have_rows('streams_item', 'option') ) : the_row();

            $streams_item_icon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" width="32" height="32"><path d="M30 0C13.458 0 0 13.458 0 30s13.458 30 30 30 30-13.458 30-30S46.542 0 30 0zm15.563 30.826l-22 15a1.002 1.002 0 0 1-1.03.058A.999.999 0 0 1 22 45V15a.999.999 0 0 1 1.564-.826l22 15a1.001 1.001 0 0 1-.001 1.652z" fill="#06bd85"/></svg>';
            $streams_item_title = get_sub_field('streams_item_title');
            $current_slug = basename(get_permalink());

            $output .= '<div class="streams-timetable__item"><div class="row no-gutters">';

            $output .= '<div class="col-2">' . $streams_item_icon . '</div>';

            $output .= '<div class="col"><div class="streams-timetable__item-date-time">' . get_sub_field('streams_item_date_time') . ' EST</div><a class="streams-timetable__item-title" ' . $streams_item_record_link . '">' . $streams_item_title . '</a></div>';

            $output .= '</div></div>';
            $i++;
        endwhile;
    }
    $output .= '</div>';
    return $output;
});
