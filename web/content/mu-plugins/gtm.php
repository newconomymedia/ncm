<?php
/**
 * Google Tag Manager
 */

// Google Tag Manager head including
function newconomy_gtm_head() {
    if ( defined( 'WP_GTM' ) ) { ?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','<?php echo WP_GTM; ?>');</script>
        <!-- End Google Tag Manager -->
    <?php }
}
add_action( 'newconomy_theme_head', 'newconomy_gtm_head', 1 );

// Google Tag Manager body including
function newconomy_gtm_body() {
    if ( defined( 'WP_GTM' ) ) { ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo WP_GTM; ?>"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <?php }
}
add_action( 'newconomy_theme_body', 'newconomy_gtm_body', 1 );
