<?php

add_shortcode('latest_stickies', function () {

    /* Get all sticky posts */
    $sticky = get_option( 'sticky_posts' );

    /* Sort the stickies with the newest ones at the top */
    rsort( $sticky );

    /* Get the 5 newest stickies (change 5 for a different number) */
    $sticky = array_slice( $sticky, 0, 5 );

    /* Query sticky posts */
    $the_query = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
    // The Loop
    if ( $the_query->have_posts() ) {
        ob_start();
        echo '<div class="ncm-articles-sticky"><h3>Editor\'s choice</h3><ul class="ncm-articles-sticky__list">';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            echo '<li class="ncm-articles-sticky__item">';
            if ( has_post_thumbnail() ) {
                the_post_thumbnail("ncm-articles-list", ['class' => "ncm-articles-sticky__item-image"]);
            }
            echo '<br><a class="ncm-articles-sticky__item-link" href="' .get_permalink(). '" title="'  . get_the_title() . '">' . get_the_title() . '</a>';
            echo '</li>';
        }
        echo '</ul></div>';

        $return = ob_get_contents(); ob_end_clean();
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    return $return;

});
