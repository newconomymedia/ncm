<?php

add_shortcode('ncmwidget_tagcloud', function() {
    if ( function_exists('wp_tag_cloud') ){
        ob_start();
        echo "<div class='tagcloud'>";
        wp_tag_cloud('unit=px&smallest=14&largest=14');
        echo "</div>";
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }
});

add_shortcode('ncmwidget_catcloud', function() {
    if ( function_exists('wp_tag_cloud') ){
        ob_start();
        echo "<div class='catcloud'>";
        wp_tag_cloud('&taxonomy=category&unit=px&smallest=14&largest=14');
        echo "</div>";
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }
});

add_shortcode('ncmwidget_instabutton', function() {
    return '<a class="instabutton" href="https://instachange.net/?utm_source=newconomy&utm_medium=main&utm_campaign=instabutton"></a>';
});

add_shortcode('ncmwidget_ticker_tape', function() { ?>
    <!-- TradingView Widget BEGIN -->
    <div class="tradingview-widget-container">
        <div class="tradingview-widget-container__widget"></div>
        <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
            {
                "symbols": [
                    {
                        "title": "BTC/USD",
                        "proName": "BITFINEX:BTCUSD"
                    },
                    {
                        "title": "ETH/USD",
                        "proName": "BITFINEX:ETHUSD"
                    },
                    {
                        "title": "EUR/USD",
                        "proName": "FX_IDC:EURUSD"
                    },
                    {
                        "title": "S&P 500",
                        "proName": "SP:SPX"
                    },
                    {
                        "title": "The Global Dow",
                        "proName": "DJ:GDOW"
                    }
                ],
                "theme": "dark",
                "isTransparent": true,
                "displayMode": "adaptive"
            }
        </script>
    </div>
    <!-- TradingView Widget END -->
<?php });

add_shortcode('ncmwidget_prices', function() {
    return "
    <!-- TradingView Widget BEGIN -->
    <div class=\"tradingview-widget-container\">
      <div class=\"tradingview-widget-container__widget\"></div>
      <script type=\"text/javascript\" src=\"https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js\" async>
      {
      \"showChart\": false,
      \"locale\": \"en\",
      \"width\": \"100%\",
      \"height\": \"440\",
      \"tabs\": [
        {
          \"title\": \"Rates\",
          \"symbols\": [
            {
              \"s\": \"BITFINEX:BTCUSD\"
            },
            {
              \"s\": \"BITFINEX:ETHUSD\"
            },
            {
              \"s\": \"FX_IDC:EURUSD\"
            },
            {
              \"s\": \"TVC:GOLD\"
            },
            {
              \"s\": \"TVC:UKOIL\"
            },
            {
              \"s\": \"SP:SPX\"
            },
            {
              \"s\": \"DJ:GDOW\"
            }
          ],
          \"originalTitle\": \"Rates\"
        }
      ]
    }
      </script>
    </div>
    <!-- TradingView Widget END -->
    ";
});
