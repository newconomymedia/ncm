<?php

add_image_size( 'ncm-articles-banner', 880, 480, true );
add_image_size( 'ncm-articles-list', 220, 140, true );
add_image_size( 'ncm-articles-small-square', 100, 100, true );

