<?php

add_action( 'modal_survey_action_init', function ($params) {
    session_start();
    $_SESSION['voting_results'] = [];
});

add_action( 'modal_survey_action_participant_vote', function ($params) {
    session_start();
    $_SESSION['voting_results'][] = $params;
});

add_action( 'modal_survey_action_autoresponse', function ($params) {
    session_start();
    print_r($_SESSION['voting_results']);

    $results = $_SESSION['voting_results'];

    // Initialize curl
    $curl = curl_init();

    // Configure curl options
    $data = [
        'name' => $results[0]['ip'],
        'surname' => '',
        'id' => 'newconomy-' . $results[0]['sid'],
        'area1' => $results[0]['aid'],
        'area2' => $results[1]['aid'],
        'area3' => $results[2]['aid'],
        'area4' => $results[3]['aid'],
        'area5' => $results[4]['aid'],
        'email' => $params['recipient']
    ];
    $jsonEncodedData = json_encode($data);
    $opts = array(
        CURLOPT_URL             => 'https://hooks.zapier.com/hooks/catch/612190/n6mjd0/',
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_CUSTOMREQUEST   => 'GET',
        CURLOPT_POST            => 1,
        CURLOPT_POSTFIELDS      => $jsonEncodedData,
        CURLOPT_HTTPHEADER  => array('Content-Type: application/json','Content-Length: ' . strlen($jsonEncodedData))
    );

    // Set curl options
    curl_setopt_array($curl, $opts);

    // Get the results
    $result = curl_exec($curl);

    // Close resource
    curl_close($curl);

    echo $result;
});
