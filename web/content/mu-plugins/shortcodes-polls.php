<?php

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function get_poll_results_by_slug($slug) {
    $request = 'https://platform.newconomy.media/api/polls/results/' . $slug;
    $response = wp_remote_get($request);
    if (is_wp_error($response)) {
        echo $response->get_error_message();
    }
    return [
        'response_code' => wp_remote_retrieve_response_code($response),
        'response_body' => json_decode(wp_remote_retrieve_body($response), true)
    ];
}

add_shortcode('poll-experts-first-results', function() {
    $date = new \DateTime();
    $date->modify('monday this week');
    $current_monday = $date->format('dmY');
    $results = get_poll_results_by_slug('experts-first-' . $current_monday);
    $sum = array_sum($results['response_body']);
    ob_start();
    echo "<table class='table-poll-results minimalistBlack'><thead><tr><th>Expert</th><th>Score</th></tr></thead><tbody>";
    foreach ($results['response_body'] as $name => $result) {
        echo "<tr><td><a target='_blank' href='/experts/" . slugify($name) . "'>" . $name . "</a></td><td>" . round ($result/$sum*100, 2) . " %</td></tr>";
    }
    echo "</tbody></table>";
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
});

add_shortcode('poll-companies-first-results', function() {
    $date = new \DateTime();
    $date->modify('monday this week');
    $current_monday = $date->format('dmY');
    $results = get_poll_results_by_slug('companies-first-' . $current_monday);
    $sum = array_sum($results['response_body']);
    ob_start();
    echo "<table class='table-poll-results minimalistBlack'><thead><tr><th>Company</th><th>Score</th></tr></thead><tbody>";
    foreach ($results['response_body'] as $name => $result) {
        echo "<tr><td><a target='_blank' href='/companies/" . slugify($name) . "'>" . $name . "</a></td><td>" . round ($result/$sum*100, 2) . " %</td></tr>";
    }
    echo "</tbody></table>";
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
});
