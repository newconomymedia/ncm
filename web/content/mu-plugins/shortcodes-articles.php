<?php

add_shortcode('ncm-top-tags', function() {
    $output = '<ul class="ncm-top-tags">';
    $top_tags = get_field('articles_options_top_tags', 'option');
    foreach ($top_tags as $top_tags_item) {
        $output .= '<li class="ncm-top-tags__item">';
        $output .= '<a class="ncm-top-tags__item-link" href="/tag/' . $top_tags_item->slug . '">#' . $top_tags_item->name . '</a>';
        $output .= '</li>';
    }
    $output .= '</ul>';
    return $output;
});

add_shortcode('ncm-articles-banner', function() {

    /*
    * Easy Query Shortcode
    * [easy_query paging="false" posts_per_page="5" post_type="post"]
    */

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => array('post'),
        'post_status' => 'publish',
        'ignore_sticky_posts' => true,
        'order' => 'DESC',
        'orderby' => 'date',
        'posts_per_page' => 1,
        'paged' => $paged,
    );

    // WP_Query
    $eq_query = new WP_Query( $args );
    if ($eq_query->have_posts()) : // The Loop
        $eq_count = 0;
        $second_col = '';
        while ($eq_query->have_posts()): $eq_query->the_post();
            $eq_count++;
            if ($eq_count === 1) { ?>
                <?php ob_start(); ?>
                <div class="<?php echo 'ncm-articles-banner__item ncm-articles-banner__item--first'; if (!has_post_thumbnail()) { ?> no-img<?php } ?>">
                    <?php if ( has_post_thumbnail() ) {
                        the_post_thumbnail("ncm-articles-banner", ['class' => "ncm-articles-banner__item-image ncm-articles-banner__item-image--first"]);
                    }?>
                    <div class="ncm-articles-banner__item-content">
                        <h3 class="ncm-articles-banner__item-title ncm-articles-banner__item-title--first">
                            <a class="ncm-articles-banner__item-link ncm-articles-banner__item-link--first" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h3>
                    </div>
                </div>
                <?php $first_col = ob_get_contents(); ob_end_clean(); ?>
            <?php } else { ?>
                <?php ob_start(); ?>
                <div class="<?php echo 'ncm-articles-banner__item ncm-articles-banner__item--next ncm-articles-banner__item--'.$eq_count; if (!has_post_thumbnail()) { ?> no-img<?php } ?> u-align-items-center">
                    <div class="ncm-articles-banner__item-inner">
                        <h3 class="ncm-articles-banner__item-title"><a class="ncm-articles-banner__item-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                </div>
                <?php $second_col .= ob_get_contents(); ob_end_clean(); ?>
            <?php } ?>
        <?php endwhile; wp_reset_query(); ?>
    <?php endif;
    return '<div class="ncm-articles-banner">
              <div class="ncm-articles-banner__list l-row">
                <div class="l-col-12">'. $first_col . '</div>
              </div>
            </div>
            ';
});

add_shortcode('ncm-related-posts', function() {

    global $post;

    $tags = wp_get_post_tags($post->ID);

    $tag_ids = array();

    if ($tags) {
        foreach ( $tags as $individual_tag ) {
            $tag_ids[] = $individual_tag->term_id;
        }
    }

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => array('post'),
        'post_status' => 'publish',
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'order' => 'DESC',
        'orderby' => 'date',
        'posts_per_page' => 6,
        'paged' => $paged,
    );

    // WP_Query
    $eq_query = new WP_Query( $args );
    ob_start();
    if ($eq_query->have_posts()) : // The Loop
        $eq_count = 0;
        ?>
        <div class="ncm-related-posts paging-style-default grey">
            <h3><?php echo __('Related posts', 'theme'); ?></h3>
            <div class="wp-easy-query-posts">
                <ul class="ncm-related-posts-list l-row">
                    <?php
                    while ($eq_query->have_posts()): $eq_query->the_post();
                        $eq_count++;
                        ?>
                        <li class="l-col-6 ncm-related-posts-list__item <?php echo (!has_post_thumbnail()) ? 'no-img ' : ''; ?><?php eq_is_first($eq_count); ?>">
                            <div class="l-row">
                                <?php if ( has_post_thumbnail() ) {
                                    echo '<div class="l-col-auto">';
                                    the_post_thumbnail('thumbnail');
                                    echo '</div>';
                                }?>
                                <div class="l-col">
                                    <h6 class="ncm-related-posts-list__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
                                    <p class="entry-meta">
                                        <?php the_time(apply_filters('eq_date_format', 'F d, Y')); ?>
                                    </p>
                                </div>
                            </div>
                        </li>

                    <?php endwhile; wp_reset_query(); ?>
                </ul>
            </div>
        </div>
    <?php endif;
    $output = ob_get_contents(); ob_end_clean();
    return $output;
});

