<?php

// Import PostTypes
use PostTypes\PostType;

// Create a People PostType
$people = new PostType(
    [
        'name' => 'people',
        'singular' => 'Person',
        'plural' => 'People',
        'slug' => 'person'
    ],
    [
        'capability_type'    => 'page',
        'supports' => ['title','editor','thumbnail'],
        'menu_position' => 5,
        'menu_icon' => 'dashicons-businessman',
        'rewrite' => ['with_front' => false]
    ]
);

$people->register();

// Create a Markets PostType
$people = new PostType(
    [
        'name' => 'markets',
        'singular' => 'Market',
        'plural' => 'Markets',
        'slug' => 'markets'
    ],
    [
        'capability_type'    => 'page',
        'supports' => ['title','editor','thumbnail'],
        'menu_position' => 5,
        'menu_icon' => 'dashicons-chart-area',
        'show_in_rest' => true,
        'rewrite' => ['with_front' => false],
    ]
);

$people->register();

// Create a Events PostType
$people = new PostType(
    [
        'name' => 'events',
        'singular' => 'Event',
        'plural' => 'Events',
        'slug' => 'events'
    ],
    [
        'capability_type'    => 'page',
        'supports' => ['title','editor','thumbnail'],
        'menu_position' => 5,
        'menu_icon' => 'dashicons-chart-area',
        'show_in_rest' => true,
        'rewrite' => ['with_front' => false],
        'has_archive' => true
    ]
);

$people->register();
