'use strict';

const marketMap = (() => {
  let fileResult,
    colorUp = '#06bd85',
    colorDown = '#f13379',
    arrowUpClass = 'icon-arrow--up',
    arrowDownClass = 'icon-arrow--down',
    minCapForShowMobileCircle = 5000000000,
    marketsParam = [], // main array
    marketCirclesCapValues = [],
    marketCirclesCapValues14Days = [],
    circleBubble,
    circleSvg,
    marketState = {
      currentShowMarket: ''
    },
    is_mobile = (window.innerWidth < 1200) ? true : false,
    marketsBubble = {
      element: document.querySelector('.markets-bubble'),
      classNameVisible: 'visible',

      showBubble: function (param) { // param = {name, r, x, y}
        const
          bubbleHeight = this.element.getBoundingClientRect().height;

        this.element.style.top = (param.y - bubbleHeight - param.r - 38) + 'px';
        this.element.style.left = param.x + 'px';
        this.element.classList.add(this.classNameVisible);
      },

      hideBubble: function () {
        this.element.classList.remove(this.classNameVisible);
      },

    };

  let init = () => {
    d3.csv(fileResult, setData);
    setQuestionMobileBlock();
    setChangeDescriptionByClick();
    setShowAllMarketsByClick();
    // showAllCoinsBtnEvent();
    showPollBtnEvent();
    showLastUpdatedMarketsData();
  };

  let setData = (dataResult) => {

    let
      tableSource = dataResult,
      marketCirclesCapValuesChildren = [],
      marketCirclesCapValues14DaysChildren = [],
      marketCircleOther = {
        category: 'Others', // Name
        slug: 'others', // Slug
        num: 0,
        content: '', // Description
        img: '', // Image
        coins: '', // Coins
        change: 0, // Change 24h
        capValue: 0, // Cap
        capValueMlrd: 0, // Cap mlrd
        capValue14Days: 0, // Cap trend 14 days
        change14Days: 0, // Trend 14 days
        opacityCircle: 0,
        opacityCircle14Days: 0,
      },
      marketChangeOther = 0,
      marketChangeOther14Days = 0,
      changeMin = Infinity,
      changeMax = -Infinity,
      changeMin14 = Infinity,
      changeMax14 = -Infinity;

    tableSource
      .forEach((el, index) => {

        let marketParam = {
          category: '', // Name
          slug: '', // Slug
          link: '', // Market link
          num: '0',
          content: '', // Description
          img: '', // Image
          coins: '', // Coins
          change: '', // Change 24h
          capValue: '', // Cap
          capValueMlrd: '', // Cap mlrd
          capValue14Days: '', // Cap trend 14 days
          change14Days: '', // Trend 14 days
          opacityCircle: '',
          opacityCircle14Days: '',
        };

        marketParam.category = el['Category'];

        if (marketParam.category == 'Unclear') return;

        marketParam.slug = convertToSlug(marketParam.category);
        // marketParam.num = (index) + 1;
        marketParam.coins = el['Cryptos'];
        marketParam.change = el['Average Change (24h), %'];
        marketParam.capValue = el['Market Cap, $'];
        marketParam.capValueMlrd = (parseInt(marketParam.capValue) / 1000000000).toFixed(2) + ' mlrd';
        marketParam.capValue14Days = el['Plus 14 days Cap, $'];
        marketParam.change14Days = (marketParam.capValue14Days / (marketParam.capValue / 100) - 100).toFixed(2);

        if (is_mobile && parseInt(marketParam.capValue) < minCapForShowMobileCircle) {
          marketCircleOther.change = ((parseFloat(marketCircleOther.change) + parseFloat(marketParam.change)) / 2).toFixed(2);
          marketCircleOther.capValue = ((parseFloat(marketCircleOther.capValue) + parseFloat(marketParam.capValue))).toFixed(2);
          marketCircleOther.capValueMlrd = (parseInt(marketCircleOther.capValue) / 1000000000).toFixed(2) + ' mlrd';
          marketCircleOther.capValue14Days = ((parseFloat(marketCircleOther.capValue14Days) + parseFloat(marketParam.capValue14Days))).toFixed(2);
          marketCircleOther.change14Days = ((parseFloat(marketCircleOther.change14Days) + parseFloat(marketParam.change14Days)) / 2).toFixed(2);
          marketChangeOther = (parseFloat(marketChangeOther) + parseFloat(marketCircleOther.change)) / 2;
          marketChangeOther14Days = (parseFloat(marketChangeOther14Days) + parseFloat(marketCircleOther.change14Days)) / 2;

        } else {
          marketsParam.push(marketParam);
        }

      });

    if (marketCircleOther.capValue > 0) {
      marketsParam.push(marketCircleOther);
    }

    let
      opacity,
      opacity14;

    // array for calculate market position in the table
    let marketsParamSorted = marketsParam.map((el) => {
        return {
          slug: el.slug,
          capValue: el.capValue
        };
      })
      .sort((a, b) => {
        return b.capValue - a.capValue;
      });

    marketsParam.forEach((el) => {
      if (parseFloat(el.change) < changeMin) changeMin = el.change;
      if (parseFloat(el.change) > changeMax) changeMax = el.change;
      if (parseFloat(el.change14Days) < changeMin14) changeMin14 = el.change14Days;
      if (parseFloat(el.change14Days) > changeMax14) changeMax14 = el.change14Days;
    });

    marketsParam.forEach((el) => {

      // calculate market position in the table
      marketsParamSorted.forEach((item, i) => {
        if (el.slug == item.slug) {
          el.num = (i) + 1;
          return;
        }
      });

      if (el.change > 0) {
        opacity = ((el.change * 100) / changeMax / 100).toFixed(2);
      } else {
        opacity = (-1) * ((el.change * 100) / changeMin / 100).toFixed(2);
      }
      el.opacityCircle = opacity;

      if (el.change14Days > 0) {
        opacity14 = ((el.change14Days * 100) / changeMax14 / 100).toFixed(2);
      } else {
        opacity14 = (-1) * ((el.change14Days * 100) / changeMin14 / 100).toFixed(2);
      }
      el.opacityCircle14Days = opacity14;

      marketCirclesCapValuesChildren
        .push({
          name: el.category,
          slug: el.slug,
          num: el.num,
          change: el.change,
          opacity: opacity,
          cap: el.capValue,
          value: Math.sqrt(el.capValue / Math.PI).toFixed(2)
        });
      marketCirclesCapValues14DaysChildren
        .push({
          name: el.category,
          slug: el.slug,
          num: el.num,
          change: el.change14Days,
          opacity: opacity14,
          cap: el.capValue14Days,
          value: Math.sqrt(el.capValue14Days / Math.PI).toFixed(2)
        });

    });

    marketCirclesCapValues['children'] = marketCirclesCapValuesChildren;
    marketCirclesCapValues14Days['children'] = marketCirclesCapValues14DaysChildren;

    makeMarketTable(marketsParam);
    makeCircles(marketsParam);
    setMarketsDescription();

  };

  let setMarketsDescription = () => {
    let requestMarket = new XMLHttpRequest();
    requestMarket.open('GET', '/wp-json/wp/v2/markets/?_embed&per_page=100', true);
    requestMarket.addEventListener('readystatechange', () => {
      if ((requestMarket.readyState == 4) && (requestMarket.status == 200)) {
        let responseMarket = JSON.parse(requestMarket.response);
        if (responseMarket.length) {
          responseMarket.forEach(response1 => {
            marketsParam.forEach(el => {
              if (el.slug == response1.slug) {
                el.content = response1.content.rendered;
                el.link = response1.link;
                if ('_embedded' in response1) {
                  el.img = response1._embedded['wp:featuredmedia'][0].source_url;
                }
                return;
              }
            });
          });
        }
      }
    });
    requestMarket.send();
  }

  let makeMarketTable = (marketsArray) => {
    // make market table
    let marketsTable = document.querySelector(".markets-table tbody"),
      arrowClassMod;

    marketsArray
      .sort((a, b) => {
        return b.capValue - a.capValue;
      })
      .forEach((market, index) => {

        let
          marketsTableElement = document.createElement('tr'),
          marketName = market.category;

        if (is_mobile) {
          marketName = market.num + '. ' + marketName;
        }

        marketsTableElement.classList.add("markets-table__item");
        if (index > 4) {
          marketsTableElement.classList.add("d-none");
        }

        if (market.change < 0) {
          arrowClassMod = arrowDownClass;
        } else {
          arrowClassMod = arrowUpClass;
        }

        marketsTableElement.innerHTML =
          '<td class="markets-table__market"><a class="markets-table__link ajax-chart-link" data-target="' + convertToSlug(market.category) + '">' + marketName + '</a></td>' +
          '<td class="markets-table__cap"><span class="markets-table__cap-value">' + market.capValueMlrd + '</span></td>' +
          '<td class="markets-table__change">' + Math.abs(market.change) + '<i class="icon-arrow ' + arrowClassMod + '"></i></td>';
        marketsTable.append(marketsTableElement);

      });
  }

  let setQuestionMobileBlock = () => {
    let iconQuestion = document.querySelector('.icon-question');
    let legend = document.querySelector('.markets-how-read');
    let legendClone = legend.cloneNode(true);
    iconQuestion.appendChild(legendClone)
    iconQuestion.addEventListener('click', e => {
      e.preventDefault();
      iconQuestion.classList.toggle('opened');
    });
  }

  let setChangeDescriptionByClick = () => {
    let dataset1 = document.getElementById('dataset1');
    let dataset2 = document.getElementById('dataset2');
    let mapDescriptionToday = document.querySelector('.markets-map__description-text--today');
    let mapDescriptionTrend = document.querySelector('.markets-map__description-text--trend');

    dataset1.addEventListener('click', e => {
      e.preventDefault();
      dataset1.classList.add('markets-map-buttons__button--active');
      dataset2.classList.remove('markets-map-buttons__button--active');
      mapDescriptionToday.classList.add('active');
      mapDescriptionTrend.classList.remove('active');
      changebubble(marketCirclesCapValues, 'today');
    });

    dataset2.addEventListener('click', e => {
      e.preventDefault();
      dataset1.classList.remove('markets-map-buttons__button--active');
      dataset2.classList.add('markets-map-buttons__button--active');
      mapDescriptionToday.classList.remove('active');
      mapDescriptionTrend.classList.add('active');
      changebubble(marketCirclesCapValues14Days, 'trend');
    });
  }

  let setShowAllMarketsByClick = () => {
    document.getElementById('markets-table-more').addEventListener('click', e => {
      let allMarketsTableItems = document.querySelectorAll('.markets-table__item');
      e.preventDefault();
      for (let i = 5; i < allMarketsTableItems.length; i++) {
        allMarketsTableItems[i].classList.toggle('d-none');
      }

      let curLabel = e.target.innerText || e.target.textContent;

      if (curLabel == e.target.getAttribute('data-all')) {
        e.target.textContent = e.target.getAttribute('data-less');
      } else {
        e.target.textContent = e.target.getAttribute('data-all');
      }

    });
  }

  let setScrollToMarketByClick = () => {
    let svgAllLinks = document.querySelectorAll('.ajax-chart-link');

    svgAllLinks.forEach(el => {
      el.addEventListener('click', e => {
        e.preventDefault();
        let marketSlug = '';

        if (e.target.tagName == 'A') {
          marketSlug = e.target.getAttribute("data-target");
        } else {
          let elemParent = closestByClass(e.target, '.ajax-chart-link');
          marketSlug = elemParent.getAttribute("data-target");
          marketSlug = convertToSlug(marketSlug);
        }
        if (marketSlug == 'others') return;
        smoothScroll('.js-scroll-market-description');
        showMarketDescription(marketSlug);
      });
    });
  }
  /*
    let showAllCoinsBtnEvent = () => {

      document.querySelector('.market-description__btn').addEventListener('click', e => {
        e.preventDefault();
        let parentCoins = closestByClass(e.target, '.market-description__coins');
        if (parentCoins) {
          parentCoins.classList.toggle('opened');
          let curLabel = e.target.innerText || e.target.textContent;
          if (curLabel == e.target.getAttribute('data-all')) {
            e.target.textContent = e.target.getAttribute('data-less');
          } else {
            e.target.textContent = e.target.getAttribute('data-all');
          }
        }
      });
    }
  */
  let showPollBtnEvent = () => {
    document.querySelector('.market-vote__btn').addEventListener('click', e => {
      e.preventDefault();
      let poolBody = e.target.parentNode.nextSibling;
      if (poolBody.nodeName == '#text') poolBody = poolBody.nextSibling;
      poolBody.classList.toggle('d-none');
    });
  }

  let showMarketDescription = (slug) => {
    let ajaxMarketHeader = document.querySelector('.market-description'),
      ajaxMarketTitle = document.querySelector('.market-description__title'),
      ajaxMarketDescription = document.querySelector('.market-description__description'),
      ajaxMarketCoins = document.querySelector('.market-description__coins-items'),
      ajaxMarketImg = document.querySelector('.market-description__img'),
      ajaxMarketPostsSeeAll = document.querySelector('.market-posts__see-all'),
      ajaxMarketReadMoreBtn = document.querySelector('.market-description__btn');

    let curMarketFilter = marketsParam.filter((el) => {
      return el.slug == slug;
    });

    if (!curMarketFilter.length) return;

    let curMarket = curMarketFilter[0];

    console.log(curMarket);

    if (marketState.currentShowMarket == curMarket.slug) return;

    marketState.currentShowMarket = curMarket.slug;
    ajaxMarketTitle.innerHTML = curMarket.category;
    ajaxMarketPostsSeeAll.setAttribute('href', '/tag/' + curMarket.slug);
    ajaxMarketDescription.innerHTML = curMarket.content;
    ajaxMarketCoins.innerHTML = curMarket.coins;
    if (curMarket.link) {
      ajaxMarketReadMoreBtn.classList.remove('d-none');
      ajaxMarketReadMoreBtn.setAttribute('href', curMarket.link);
    } else {
      ajaxMarketReadMoreBtn.classList.add('d-none');
      ajaxMarketReadMoreBtn.setAttribute('href', '#');
    }
    if (curMarket.img) {
      ajaxMarketImg.innerHTML = '<img src="' + curMarket.img + '" alt="">';
      ajaxMarketHeader.classList.add('with-img');
    } else {
      ajaxMarketImg.innerHTML = '';
      ajaxMarketHeader.classList.remove('with-img');
    }
    ajaxMarketHeader.classList.remove('d-none');
    showRelativePosts(curMarket.slug);
  }

  let showRelativePosts = (slug) => {
    if (!slug) return;
    let ajaxMarketPosts = document.querySelector('.ajax-market-posts');
    let ajaxMarketPostWrapperClass = 'market-post';
    let ajaxMarketPostBodyClass = 'market-posts__body';
    let ajaxMarketPostTitleClass = 'market-post__title';
    let ajaxMarketPostImgClass = 'market-post__img';
    // let ajaxMarketPostContentClass = 'market-post__content';
    let ajaxMarketPostFail = '<div class="market-post__fail">Nothing to show</div>';
    let ajaxMarketPostBody = document.querySelector('.' + ajaxMarketPostBodyClass);

    blockLoading(ajaxMarketPostBody, true);
    ajaxMarketPosts.classList.remove('d-none');

    let requestTagID = new XMLHttpRequest();
    requestTagID.open('GET', '/wp-json/wp/v2/tags?slug=' + convertToSlug(slug), true);
    requestTagID.addEventListener('readystatechange', () => {

      if (requestTagID.readyState != 4) return;
      if (requestTagID.status == 200) {
        let responseTagID = JSON.parse(requestTagID.response);

        if (responseTagID.length) {

          let requestTag = new XMLHttpRequest();
          requestTag.open('GET', '/wp-json/wp/v2/posts/?tags=' + responseTagID[0].id + '&_embed', true);
          requestTag.addEventListener('readystatechange', () => {

            if (requestTag.readyState != 4) return;
            if (requestTag.status == 200) {
              let responseTag = JSON.parse(requestTag.response);

              if (responseTag.length) {
                ajaxMarketPostBody.innerHTML = '';
                responseTag.every((el, indx) => {
                  let divMarketPost = document.createElement('div'),
                    postContent = '',
                    postImg = '';
                  divMarketPost.className = ajaxMarketPostWrapperClass;
                  if ('_embedded' in el) {
                    postImg = 'style="background-image:url(' + el._embedded['wp:featuredmedia'][0].source_url + ')"';
                  }
                  postContent += '<a href="' + el.link + '">';
                  postContent += '<div class="' + ajaxMarketPostImgClass + '" ' + postImg + '></div>';
                  postContent += '<div class="' + ajaxMarketPostTitleClass + '">' + el.title.rendered + '</div>';
                  postContent += '</a>';
                  // postContent += '<div class="' + ajaxMarketPostContentClass + '">' + el.excerpt.rendered + '</div>';
                  divMarketPost.innerHTML = postContent;
                  ajaxMarketPostBody.appendChild(divMarketPost);
                  return indx < 2;
                });
                ajaxMarketPosts.classList.remove('d-none');
              } else {
                requestFail(1);
              }
            } else {
              requestFail(2);
            }
          });
          requestTag.send();
        } else {
          requestFail(3);
        }
      } else {
        requestFail(4);
      }
    });
    requestTagID.send();

    let requestFail = (key) => {
      ajaxMarketPostBody.innerHTML = ajaxMarketPostFail;
      setTimeout(() => {
        ajaxMarketPosts.classList.add('d-none');
      }, 5000);
    }
  }

  let makeCircles = () => {

    let chartSize = (window.innerWidth < 370) ? 260 : (window.innerWidth < 500) ? 345 : 480;

    circleBubble = d3.layout.pack()
      .sort(null)
      .size([chartSize, chartSize])
      .padding(15);

    circleSvg = d3.select("#chart").append("svg")
      .attr("width", chartSize)
      .attr("height", chartSize)
      .attr("class", "bubble");

    // initial state

    let node = circleSvg.selectAll(".node")
      .data(circleBubble.nodes(marketCirclesCapValues)
        .filter((d) => {
          return !d.children;
        }))
      .enter().append("g")
      .attr("class", (d) => {
        return "node ajax-chart-link " + convertToSlug(d.slug)
      })
      .attr("data-target", (d) => {
        return convertToSlug(d.slug)
      })
      .attr("transform", (d) => {
        return "translate(" + d.x + "," + d.y + ")";
      });

    node.append("circle")
      .attr("r", (d) => {
        return d.r;
      })
      .attr("class", "fillcolor");

    node.append("circle")
      .attr("r", (d) => {
        return d.r;
      })
      .attr("class", "circle-market")
      .style("fill", (d) => {
        let color = colorUp;
        if (d.change < 0) color = colorDown;
        return color;
      })
      .style("fill-opacity", (d) => {
        let opacity;
        if (d.change < 0)
          opacity = (-1) * d.opacity;
        else
          opacity = d.opacity;
        return opacity;
      });

    node.append("foreignObject")
      .attr("class", "circle-desc")
      .attr("width", (d) => {
        return parseInt(d.r * 2);
      })
      .attr("height", (d) => {
        return parseInt(d.r * 2);
      })
      .attr('x', (d) => {
        return parseInt(d.r * (-1));
      })
      .attr('y', (d) => {
        return parseInt(d.r * (-1));
      })
      .style({
        'background': 'transparent',
      })
      .html((d) => {
        let name = d.name;
        if (is_mobile) {
          name = d.num
        }
        return '<span class="circle-desc__text hidden">' + name + '</span><span class="circle-desc__helper"></span>';
      });

    document.querySelectorAll('.circle-desc__text').forEach((el) => {
      if ((el.offsetWidth <= el.parentNode.getAttribute('width') && el.offsetHeight <= el.parentNode.getAttribute('height')) === true) {
        el.classList.remove('hidden');
      }
    });

    let
      chart = document.getElementById('chart'),
      chartCircles = chart.querySelectorAll('.ajax-chart-link'),
      tooltip = document.createElement('div');

    tooltip.className = 'circle-tooptip';

    tooltip.innerHTML = '<span class="circle-tooptip__title">Category:</span><span class="circle-tooptip__value circle-tooptip__name"></span><br>' +
      '<span class="circle-tooptip__title">(24h), %:</span><span class="circle-tooptip__value circle-tooptip__change"></span><br>' +
      '<span class="circle-tooptip__title">Cap, $</span><span class="circle-tooptip__value circle-tooptip__cap"></span>';
    chart.append(tooltip);

    let
      circleTooptipName = document.querySelector('.circle-tooptip__name'),
      circleTooptipChange = document.querySelector('.circle-tooptip__change'),
      circleTooptipCap = document.querySelector('.circle-tooptip__cap');

    if (!is_mobile) {

      chartCircles.forEach(el => {

        el.onmouseenter = (e => {
          e.preventDefault();
          let
            market_slug = e.target.getAttribute('data-target'),
            marketParams = getMarketParamsBySlug(market_slug);

          marketsBubble.hideBubble();
          if (!marketParams) return;
          circleTooptipName.innerHTML = marketParams.name;
          circleTooptipChange.innerHTML = marketParams.change + '%';
          circleTooptipCap.innerHTML = '$' + (parseInt(marketParams.cap) / 1000000000).toFixed(2) + ' mlrd';

          let coordTarget = e.target.getBoundingClientRect();
          let coordChart = chart.getBoundingClientRect();

          tooltip.style.top = coordTarget.top - coordChart.top - 100 + 'px';
          tooltip.style.left = coordTarget.left - coordChart.left + 'px';
          tooltip.style.display = 'block';
        });

        el.onmouseleave = (e => {
          e.preventDefault();
          circleTooptipName.innerHTML = '';
          circleTooptipChange.innerHTML = '';
          circleTooptipCap.innerHTML = '';
          tooltip.style.display = 'none';
        });
      });

    }
    setScrollToMarketByClick();
  }

  // Update function
  let changebubble = (root, targetBtn) => {
    let node = circleSvg.selectAll(".node")
      .data(circleBubble.nodes(root)
        .filter((d) => {
          return !d.children;
        }));

    node
      .select('.circle-market')
      .transition().duration(500)
      .attr("r", (d) => {
        return d.r;
      })
      .style("fill", (d) => {
        let color = colorUp;
        if (d.change < 0) color = colorDown;
        return color;
      })
      .style("fill-opacity", (d) => {
        let opacity;

        if (d.change < 0)
          opacity = (-1) * d.opacity;
        else
          opacity = d.opacity;

        if (!is_mobile && opacity == 1 && d.change > 0) {
          let {name, r, x, y} = d;
          if (targetBtn == 'trend')
            marketsBubble.showBubble({name, r, x, y});
          else
            marketsBubble.hideBubble();
        }

        return opacity;
      });

    node
      .select('.fillcolor')
      .transition().duration(500)
      .attr("r", (d) => {
        return d.r;
      });

  }

  function showLastUpdatedMarketsData() {
    var xhr = new XMLHttpRequest();
    xhr.open("HEAD", fileResult, true);
    xhr.send(null);
    xhr.onreadystatechange = function () {
      if (xhr.readyState != 4) return;
      if (xhr.status != 200) return
      let
        lastModified,
        curDate,
        fileDate,
        diffDate,
        diffTime = {};

      lastModified = xhr.getResponseHeader("Last-Modified");
      if (!lastModified) return;
      curDate = Date.now();
      fileDate = new Date(lastModified);
      diffDate = ~~((curDate - fileDate.getTime()) / 1000);
      if (!diffDate || diffDate < 0) return;
      diffTime.days = ~~(diffDate / 86400);
      diffTime.hours = ~~((diffDate - diffTime.days * 86400) / 3600);
      diffTime.minuts = ~~((diffDate - diffTime.days * 86400 - diffTime.hours * 3600) / 60);
      showLastUpdatedMarketsHtml(diffTime);
    }
  }

  function showLastUpdatedMarketsHtml(diffTime) {
    if (!diffTime) return;
    let
      outHtml,
      marketsTotal = document.querySelector('.markets-total');

    if (!marketsTotal) return;
    outHtml = '<div class="markets-total__updated">Updated ';
    if (diffTime.days) outHtml += diffTime.days + 'd ';
    if (diffTime.hours) outHtml += diffTime.hours + 'h ';
    if (diffTime.minuts) outHtml += diffTime.minuts + 'm ';
    outHtml += 'ago</div>';
    marketsTotal.insertAdjacentHTML('afterBegin', outHtml);
  }


  let convertToSlug = (text) => {
    return text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
  }

  return {
    run: (file) => {
      fileResult = file;
      init();
    }
  }

  /**
   * Smooth Scroll
   *
   * @param  {string} targetClassName Класс элемента к которому скрол
   */
  function smoothScroll(targetClassName) {
    let startY = currentYPosition();
    let stopY = elmYPosition(targetClassName);
    let distance = stopY > startY ? stopY - startY : startY - stopY;
    if (distance < 100) {
      scrollTo(0, stopY);
      return;
    }
    let speed = Math.round(distance / 100);
    if (speed >= 20) speed = 20;
    let step = Math.round(distance / 25);
    let leapY = stopY > startY ? startY + step : startY - step;
    let timer = 2;
    if (stopY > startY) {
      for (let i = startY; i < stopY; i += step) {
        setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
        leapY += step;
        if (leapY > stopY) leapY = stopY;
        timer++;
      }
      return;
    }
    for (let i = startY; i > stopY; i -= step) {
      setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
      leapY -= step;
      if (leapY < stopY) leapY = stopY;
      timer++;
    }

    function currentYPosition() {
      // Firefox, Chrome, Opera, Safari
      if (self.pageYOffset) return self.pageYOffset;
      // Internet Explorer 6 - standards mode
      if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
      // Internet Explorer 6, 7 and 8
      if (document.body.scrollTop) return document.body.scrollTop;
      return 0;
    }

    function elmYPosition(targetClassName) {
      let elm = document.querySelector(targetClassName);
      let y = elm.offsetTop;
      let node = elm;
      while (node.offsetParent && node.offsetParent != document.body) {
        node = node.offsetParent;
        y += node.offsetTop;
      }
      return y;
    }
  }

  function blockLoading(el, isloading) {
    let node;
    if (typeof (el) === 'string') {
      node = document.querySelector(el);
    } else if (typeof (el) === 'object') {
      node = el;
    } else {
      return;
    }
    if (!node) return;
    if (isloading) {
      node.innerHTML = '<div class="block-loading"></div>';
    } else {
      node.innerHTM = '';
    }
  }

  function closestByClass(el, selector) {
    var matchesFn;

    // find vendor prefix
    ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
      if (typeof document.body[fn] == 'function') {
        matchesFn = fn;
        return true;
      }
      return false;
    })

    var parent;

    // traverse parents
    while (el) {
      parent = el.parentElement;
      if (parent && parent[matchesFn](selector)) {
        return parent;
      }
      el = parent;
    }

    return null;
  }

  function getMarketParamsBySlug(slug) {
    let result = {};
    if (!slug) return;
    marketsParam.forEach(el => {
      if (el.slug === slug) {
        result.name = el.category;
        result.slug = el.slug;
        result.change = el.change;
        result.cap = el.capValue;
        return;
      }
    });
    return result;
  }

})();

marketMap.run('/content/csv/result.csv');

let ready = () => {

}

document.addEventListener("DOMContentLoaded", ready);
