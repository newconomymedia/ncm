export default {
  init() {
    // JavaScript to be fired on all pages

    jQuery('.l-sidebar, .sidebar-support').theiaStickySidebar({
      // Settings
      additionalMarginTop: 100
    });

    ScrollOut({
      targets: ".sticky-header",
      offset: 200
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
