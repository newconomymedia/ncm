<li class="ncm-articles-list__item">
    <div class="l-row">
        <div class="l-col-auto"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail("ncm-articles-list", ['class' => 'ncm-image--desktop']); the_post_thumbnail("ncm-articles-small-square", ['class' => 'ncm-image--mobile']); }?></a></div>
        <div class="l-col"><h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3><div class="entry-meta"><?php the_primary_category(); ?> | <?php the_time('j F Y, g:i a'); ?></div></div>
    </div>
</li>
