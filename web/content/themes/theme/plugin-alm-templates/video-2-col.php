<li class="alm-layout <?php alm_is_odd($alm_current); ?>">
    <a href="<?php the_permalink(); ?>"><img class="more-videos__image" src="<?php the_field('image') ?>"></a>
    <div class="details">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    </div>
</li>
