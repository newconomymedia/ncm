<li class="alm-layout alm-default alm-3-col <?php alm_is_odd($alm_current); ?>">
    <a href="<?php the_permalink(); ?>"><img src="https://img.youtube.com/vi/9FIBbJgA-IY/mqdefault.jpg"></a>
    <div class="details">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <p class="entry-meta">
            <?php the_time("F d, Y"); ?>
        </p>
        <?php alm_get_excerpt(22); ?>
    </div>
</li>
