<?php

function newconomy_custom_assets() {
    $config = false;
    $config_path = get_template_directory() . '/assets/custom-assets.json';
    if (file_exists($config_path)) {
        $config = json_decode(file_get_contents($config_path), true);
    }
    foreach ($config as $config_index => $config_item) {
        if ('page' === $config_index) {
            foreach ($config_item as $config_child_index => $config_child_item) {
                if (is_page($config_child_index)) {
                    $styles = $config_child_item['styles'];
                    $scripts = $config_child_item['scripts'];
                    foreach ($styles as $styles_item) {
                        $styles_item_path = $scripts_item_path = get_template_directory() . '/assets/custom/styles/' . $styles_item . '.css';
                        $styles_item_uri = get_template_directory_uri() . '/assets/custom/styles/' . $styles_item . '.css';
                        if (file_exists($styles_item_path)) {
                            wp_enqueue_style('custom-assets-styles-' . $styles_item, $styles_item_uri);
                        }
                    }
                    foreach ($scripts as $scripts_item) {
                        $scripts_item_path = get_template_directory() . '/assets/custom/scripts/' . $scripts_item . '.js';
                        $scripts_item_uri = get_template_directory_uri() . '/assets/custom/scripts/' . $scripts_item . '.js';
                        if (file_exists($scripts_item_path)) {
                            wp_enqueue_script('custom-assets-scripts-' . $scripts_item, $scripts_item_uri, ['jquery'], false, true);
                        }
                    }
                }
            }
        }
    }
}
add_action( 'wp_enqueue_scripts', 'newconomy_custom_assets', 99999 );
