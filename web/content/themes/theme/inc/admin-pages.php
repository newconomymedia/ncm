<?php

if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Articles Options',
        'menu_title'    => 'Articles Options',
        'menu_slug'     => 'articles-options-page',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    acf_add_options_page(array(
        'page_title'    => 'Conference Timetable',
        'menu_title'    => 'Conference Timetable',
        'menu_slug'     => 'conference-timetable-page',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    acf_add_options_page(array(
        'page_title'    => 'Streams Timetable',
        'menu_title'    => 'Streams Timetable',
        'menu_slug'     => 'streams-timetable-page',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

add_action('admin_head', function () {
    echo '<style>
        .acf-postbox span.select2.select2-container.select2-container--default.select2-container--below {
            width: 100% !important;
        }
    }
    </style>';
});
