<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('fonts-fa', '//use.fontawesome.com/releases/v5.8.0/css/all.css', false, null);
    wp_enqueue_style('theme-styles-main', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('theme-scripts-main', asset_path('scripts/main.js'), ['jquery'], null, true);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

//add_action( 'wp_enqueue_scripts', function() {
//    wp_enqueue_script('d3js', 'https://d3js.org/d3.v3.min.js', ['jquery'], false, true);
//    $translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
//    wp_localize_script( 'd3js', 'marketData', $translation_array );
//});

add_action('wp_ajax_markets_map_post', 'markets_map_callback');
add_action('wp_ajax_nopriv_markets_map_post', 'markets_map_callback');

function markets_map_callback() {
    echo 'ajax';
    wp_die();
}

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    load_theme_textdomain('theme', get_template_directory() . '/languages');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'theme'),
        'categories_navigation' => __('Categories Navigation', 'theme'),
        'footer_navigation' => __('Footer Navigation', 'theme'),
        'service_navigation' => __('Service Navigation', 'theme')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'theme'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
         'name'          => __('Video', 'theme'),
         'id'            => 'sidebar-video'
    ] + $config);
    register_sidebar([
        'name'          => __('Header', 'theme'),
        'id'            => 'sidebar-header'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'theme'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    oxboot('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    oxboot()->singleton('oxboot.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    oxboot()->singleton('oxboot.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    oxboot('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});
