<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'theme') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    global $wp_query;
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("oxboot/template/{$class}/data", $data, $template);
    }, []);
    if (get_query_var('virtual_post_type') && get_query_var('virtual_post_type_slug')) {
        $virtual_post_type_slug = get_query_var('virtual_post_type_slug');
        if ('companies' === get_query_var('virtual_post_type')) {
            $company = get_company_by_slug($virtual_post_type_slug);
            if (200 === $company['response_code']) {
                $template = locate_template('single-companies.blade.php');
                $data['company_name'] = $company['response_body']['name'];
                $data['company_slug'] = $company['response_body']['slug'];
            } else {
                $wp_query->set_404();
                status_header(404);
                $template = locate_template('404.blade.php');
            }
        } elseif ('experts' === get_query_var('virtual_post_type')) {
            $expert = get_expert_by_slug($virtual_post_type_slug);
            if (200 === $expert['response_code']) {
                $template = locate_template('single-experts.blade.php');
                $data['expert_name'] = $expert['response_body']['name'];
                $data['expert_slug'] = $expert['response_body']['slug'];
                $data['expert_speciality'] = $expert['response_body']['speciality'];
                $data['expert_description'] = $expert['response_body']['description'];
            } else {
                $wp_query->set_404();
                status_header(404);
                $template = locate_template('404.blade.php');
            }
        }

    }
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);


/**
 * Display sidebar
 */
add_filter('oxboot/display_sidebar', function ($display) {
    static $display;

    isset($display) || $display = in_array(true, [
        // The sidebar will be displayed if any of the following return true
        is_single(),
        is_page('conference'),
        is_page('streams'),
        is_category(),
        is_post_type_archive(),
        is_tag(),
        is_404(),
        is_page_template('template-page-with-sidebar.php')
    ]);

    return $display;
});

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("oxboot/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);
