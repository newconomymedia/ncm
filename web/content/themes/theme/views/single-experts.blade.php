@extends('layouts.app-with-sidebar')

@section('content')
  <h1>{{ $expert_name }}</h1>
  <p>Speciality: {{ $expert_speciality }}</p>
  <p>About expert: {!! $expert_description !!}</p>
@endsection
