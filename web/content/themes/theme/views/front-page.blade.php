@extends('layouts.front-page')

@section('content')
  @while (have_posts()) @php the_post() @endphp
    {!! do_shortcode('[ajax_load_more button_label="Older Articles" post_type="post" posts_per_page="5" offset="3" destroy_after="5" css_classes="ncm-articles-all-listing" theme_repeater="default.php" preloaded="true"]') !!}
  @endwhile
@endsection
