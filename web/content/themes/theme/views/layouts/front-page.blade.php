<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class('newconomy-frontpage') @endphp>
    @php do_action('newconomy_theme_body') @endphp
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="l-wrap l-wrap--common l-wrap--front-page" role="document">
      <div class=" l-container">
        <div class="l-content l-row">
          <aside class="l-sidebar l-sidebar--front-page">
            {!! do_shortcode('[latest_stickies]') !!}
          </aside>
          <main class="l-main l-main--common l-main--front-page">
            {!! do_shortcode('[smartslider3 slider=2]') !!}
            <div class="ncm-articles ncm-articles--all">
              <div class="l-row">
                <div class="l-col-md-8 front-page-content">
                  @yield('content')
                </div>
                <div class="l-col-md-4 sidebar-support">
                  @php dynamic_sidebar('sidebar-primary') @endphp
                </div>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
