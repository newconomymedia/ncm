<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('newconomy_theme_body') @endphp
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="l-wrap l-wrap--full-width" role="document">
      <main class="l-main l-main--full-width">
        @yield('content')
      </main>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
