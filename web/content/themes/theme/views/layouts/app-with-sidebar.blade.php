<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('newconomy_theme_body') @endphp
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="l-wrap l-wrap--common" role="document">
      <div class="l-container">
        <div class="l-banner">@yield('banner')</div>
        <div class="l-content l-row">
          <main class="l-main l-main--common l-main--with-sidebar">
            @yield('content')
          </main>
          <aside class="l-sidebar l-sidebar--common">
            @php dynamic_sidebar('sidebar-primary') @endphp
          </aside>
        </div>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
