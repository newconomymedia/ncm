{{--
  Template Name: Markets Map Page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    <div class="markets-wrapper">
        <div class="markets-row">
            <div class="markets-map">
                <h1 class="markets-map__title">
                    <?php the_title(); ?>
                </h1>
                <div class="markets-map__description">
                    <span class="markets-map__description-text markets-map__description-text--today active">The comparative amount of players in the cryptocurrency market</span>
                    <span class="markets-map__description-text markets-map__description-text--trend">Trend two weeks ahead, based on retrospective data for the month</span>
                    <i class="icon-question icon-question--description"></i>
                </div>
                <div class="markets-map__indication-text">Color indication shows the intensity of the rising <i class="icon-arrow--up"></i> and falling <i class="icon-arrow--down"></i> of the market</div>
                <div class="markets-map-buttons">
                    <button id="dataset1" class="markets-map-buttons__button markets-map-buttons__button--active">Today's map</button>
                    <button id="dataset2" class="markets-map-buttons__button">Trend</button>
                </div>
                <div class="markets-map-chart">
                    <div class="markets-total">
                        <div class="markets-total__body">
                            <div class="markets-total__title">Total Cap<span class="markets-total__sum markets-total__sum_title">$207.34 bln</span></div>
                            <div class="markets-total__24vol">Total 24h Vol:<span class="markets-total__sum markets-total__sum_24vol">$14.16 bln</span></div>
                            <div class="markets-total__dominance">BTC Dominance<span class="markets-total__sum markets-total__sum_dominance">53.58%</span></div>
                        </div>
                    </div>
                    <div class="markets-how-read">
                        <div class="markets-how-read__title">How to read</div>
                        <div class="markets-indication">
                            <div class="markets-indication__text">Color indication shows change<br>of the market over the 24 h.</div>
                            <div class="markets-indication__line">
                                <span class="markets-indication__null">0</span>
                                <i class="markets-indication__arrow markets-indication__arrow--up icon-arrow--up"></i>
                                <i class="markets-indication__arrow markets-indication__arrow--down icon-arrow--down"></i>
                            </div>
                        </div>
                        <div class="markets-circles">
                            <div class="markets-circles__text">Circles are sized according to<br>the market capitalization</div>
                            <div class="markets-circles__body">
                                <div class="markets-circles__circle markets-circles__circle--big">
                                    <div class="markets-circles__circle markets-circles__circle--small"></div>
                                </div>
                                <div class="markets-circles__legend markets-circles__legend--big">20 mlrd</div>
                                <div class="markets-circles__legend markets-circles__legend--small">2 mlrd</div>
                            </div>
                        </div>
                    </div>
                    <div class="markets-map-chart__wrapper">
                        <div id="chart" class="markets-map-chart__data"></div>
                        <div class="markets-bubble">The fastest growing<br>market according to<br>short-term trends</div>
                    </div>
                </div>
            </div>
            <table class="markets-table">
                <tr>
                    <th>Market</th>
                    <th>Cap, $</th>
                    <th>(24h), %</th>
                </tr>
            </table>
            <button class="markets-table-more" id="markets-table-more" data-all="+ Show all markets" data-less="Show less">+ Show all markets</button>
        </div>
        <?php // markets-row ?>
        <div class="market-content">
            <div class="js-scroll-market-description"></div>
            <div class="market-description d-none">
                <div class="market-description__img"></div>
                <div class="market-description__text">
                    <div class="market-description__title"></div>
                    <div class="market-description__description"></div>
                    <div class="market-description__coins">
                        <div class="market-description__coins-body">
                            <div class="market-description__coins-caption">The market is formed by the following currencies.</div>
                            <div class="market-description__coins-items"></div>
                        </div>
                        <div class="market-description__show-all"><a href="#" class="market-description__btn">Read more</a></div>
                    </div>
                </div>
            </div>
            <div class="market-posts ajax-market-posts d-none">
                <div class="market-posts__caption">Related Content <span class="market-posts__line">|</span> <a href="#" class="market-posts__see-all">See All</a></div>
                <div class="market-posts__body"></div>
            </div>
        </div>
        <?php
            $poll = do_shortcode('[totalpoll id="17822"]');
            if ($poll) :
        ?>
        <div class="market-vote">
            <div class="market-vote__btn"><img src="<?= get_template_directory_uri() ?>/assets/images/market-map-vote-btn.jpg" alt=""></div>
            <div class="market-vote__body d-none"><?= $poll ?></div>
        </div>
        <?php endif; ?>
    </div>
    <div class="container">
        <div class="market-coin-tables">
            <?php
            echo do_shortcode('[coin-market-cap]');
            ?>
        </div>
    </div>

  @endwhile
@endsection
