@extends('layouts.app-video')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  {!! do_shortcode('[aiovg_video id=42529]') !!}
  <br>
  <div class="button-group" style="text-align: center">
    <a class="btn btn--strong btn--green external" href="https://daoconsensus.com/" target="_blank" rel="nofollow">Join Our Community</a>
  </div>
  <br>
  <div class="u-hide-nomobile">
    {!! do_shortcode('[streams-timetable]') !!}
  </div>
  <br>
  <div class="streams-adverts">
    {!! do_shortcode('[pro_ad_display_adzone id=37206]') !!}
  </div>
  <br>
  <div class="more-videos">
    {!! do_shortcode('[ajax_load_more_filters id="videofilter" target="videofilter_output"]') !!}
    {!! do_shortcode('[ajax_load_more id="videofilter_output" theme_repeater="video-2-col.php" post_type="aiovg_videos" taxonomy="aiovg_categories" taxonomy_terms="records" taxonomy_operator="IN" posts_per_page="6" scroll="true" button_label="Older Videos"]') !!}
  </div>
  @endwhile
@endsection
