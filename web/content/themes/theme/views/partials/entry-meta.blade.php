<i class="far fa-clock"></i>&nbsp;<time class="entry-time" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
<span class="byline author vcard">
  {{ __('|', 'theme') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
    {{ get_the_author() }}
  </a>
</span>
