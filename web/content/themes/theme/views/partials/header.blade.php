<header class="l-header">
  <div class="l-header__top">
    {!! do_shortcode('[ncmwidget_ticker_tape]') !!}
  </div>
  <div class="l-header__banner">
    @if (shortcode_exists('pro_ad_display_adzone'))
      {!! do_shortcode('[pro_ad_display_adzone id=41939]') !!}
    @endif
  </div>
  <div class="l-header__middle sticky-header">
    <div class="l-container">
      <div class="l-row u-align-items-center">
        <div class="l-col-md-auto l-header-col l-header-col--logo">
          <a class="site-logo" href="{{ home_url('/') }}">{!! do_shortcode('[svg-logo-main]') !!}{!! do_shortcode('[svg-logo-mobile]') !!}</a>
        </div>
        <div class="l-col l-header-col l-header-col--buttons-mobile">
          <div class="button-group">
            <a class="btn btn--white btn--border btn--mobile" href="https://daoconsensus.com/" target="_blank">{!!__('Club', 'theme') !!}</a>
            <a class="btn btn--white btn--border btn--mobile" href="https://newconomy.media/streams"><div class="live-flash"></div>{!!__('Live', 'theme') !!}</a>
          </div>
        </div>
        <div class="l-col-auto">
          <div class="mobile-menu-button"><span></span><span></span><span></span></div>
        </div>
        <div class="l-col l-header-col l-header-col--nav u-hide-mobile">
          <nav class="l-header__nav">
            @if (has_nav_menu('primary_navigation'))
              {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-main', 'walker' => new Ncm_Navwalker()]) !!}
            @endif
          </nav>
        </div>
        <div class="l-col-auto l-header-col l-header-col--buttons u-hide-mobile">
          @php dynamic_sidebar('sidebar-header') @endphp
        </div>
      </div>
    </div>
  </div>
  <div class="l-header__bottom">
    <div class="l-container">
      <div class="l-row  u-align-items-center">
        <div class="l-col">
          {!! do_shortcode('[ncm-top-tags]') !!}
        </div>
        <div class="l-col-auto">
          {{ get_search_form() }}
        </div>
      </div>
    </div>
  </div>
</header>
