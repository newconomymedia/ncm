<article @php post_class() @endphp>
  <header>
    @php
      if ( has_post_thumbnail() ) {
        the_post_thumbnail("ncm-articles-banner", ['class' => "u-image-fluid"]);
      }
    @endphp
    <h1 class="entry-title">{!! the_title() !!}</h1>
    @include('partials/entry-meta')
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    @php the_tags( '<ul class="entry-tags"><li>','</li><li>','</li></ul>') @endphp
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'theme'), 'after' => '</p></nav>']) !!}
    {!! do_shortcode('[ncm-related-posts]') !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
