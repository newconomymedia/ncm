<article @php post_class() @endphp>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <div class="video-meta">
    @php
      if ( has_post_thumbnail() ) {
        the_post_thumbnail("ncm-articles-banner", ['class' => "u-image-fluid"]);
      }
    @endphp
    <h1 class="entry-title entry-title--video">{!! the_title() !!}</h1>
    @include('partials/entry-meta')
    {{ __('Share video on social networks:', 'theme') }}
    <span class="entry-share">
      <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
      <script src="//yastatic.net/share2/share.js"></script>
      <div class="ya-share2" data-services="facebook,twitter,linkedin,telegram"></div>
    </span>
  </div>
  <footer>
    @php the_tags( '<ul class="entry-tags"><li>','</li><li>','</li></ul>') @endphp
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'theme'), 'after' => '</p></nav>']) !!}
  </footer>
</article>
<div class="video-banner">
  {!!  do_shortcode("[pro_ad_display_adzone id=37206]") !!}
</div>
<div class="more-videos">
  {!! do_shortcode('[ajax_load_more theme_repeater="video-2-col.php" post_type="aiovg_videos"  taxonomy="aiovg_categories" taxonomy_terms="records" taxonomy_operator="IN" posts_per_page="6" scroll="true" button_label="Older Videos"]') !!}
</div>
