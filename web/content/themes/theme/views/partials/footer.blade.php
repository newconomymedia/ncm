<footer class="l-footer">
  <div class="l-footer__top">
    <div class="l-container">
      <div class="l-row">
        <div class="l-col-md-3 l-footer__item">
          <nav class="l-footer__nav">
            @if (has_nav_menu('footer_navigation'))
              {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav nav-footer', 'walker' => new Ncm_Navwalker()]) !!}
            @endif
          </nav>
        </div>
        <div class="l-col-md-3 l-footer__item">
          @php dynamic_sidebar('sidebar-footer') @endphp
        </div>
        <div class="l-col-md-3 l-footer__item">
          {!! do_shortcode('[ncmwidget_catcloud]') !!}
        </div>
        <div class="l-col-md-3 l-footer__item">
          <img src="https://resources-en.newconomy.media/2018/04/bage__powered-by-crypterium.svg" class="attachment-full size-full" width="180px" alt="">
        </div>
      </div>
    </div>
  </div>
  <div class="l-footer__bottom"><div class="l-footer__copyright">© Copyright 2018-2019. All Rights Reserved</div></div>
</footer>
