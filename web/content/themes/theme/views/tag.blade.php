@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'theme') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  {!! do_shortcode('[ajax_load_more post_type="post" posts_per_page="10" offset="0" tag="' . get_queried_object()->slug . '" css_classes="" theme_repeater="default.php" preloaded="true"]') !!}
@endsection
